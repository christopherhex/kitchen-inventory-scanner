extern crate serialport;
extern crate reqwest;
extern crate serde;

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct ApiResponse {
    product: Product,
    status_verbose: String
}

#[derive(Serialize, Deserialize, Debug)]
struct Product {
    product_name: String
}



fn retrieve_barcode_data(barcode: &String) -> Result<(), reqwest::Error> {


    let base_url = "https://world.openfoodfacts.org/api/v0/product/".to_owned();
    let url_postfix = ".json".to_owned();

    let mut url: String = "".to_owned();
    url.push_str(&base_url);
    url.push_str(&barcode.trim());
    url.push_str(&url_postfix);

    println!("{:#?}", url);


    let resp : ApiResponse = reqwest::blocking::get(&url)?
        .json()?;

    println!("Scanned: {:#?}", resp.product.product_name);
    Ok(())
}


fn main() {
    println!("Hello, world!");    
    match serialport::open("/dev/ttyUSB0") {

        Ok(mut port) => {
            println!("Opened Port");

            let mut serial_buf: Vec<u8> = vec![0; 32];

            loop {
                match port.read(serial_buf.as_mut_slice()) {
                    Ok(_t) => {
                        let mut s = String::from_utf8_lossy(&serial_buf);
                        let s1 = s.to_mut();
                        let s2 = s1.replace(|c: char| !c.is_alphanumeric(), "");
                        retrieve_barcode_data(&s2).unwrap();
                    },
                    Err(_) => {
                        // Do nothing
                    }
                }
            }
        
        }
        Err(_e) => {
            println!("Failed to open port");
        }
    };



}
