# Kitchen Inventory Scanner
A pet project to learn more about Rust and at the same time find a way to better know what is in our kitchen cabinets.

![cover image](./img/info-image.jpg)

## Hardware
* Raspberry Pi 3b+
* [MH-ET LIVE Barcode Scanner V3.0](https://www.tinytronics.nl/shop/en/others/other/mh-et-live-barcode-scanner-v3.0)

## Software 
Currently only tested on a Raspberry Pi. The following packages need to be installed:

```
sudo apt install libudev-dev libssl-dev
```

## API
This project uses the free API from [openfoodfacts.org](https://www.openfoodfacts.org) to get product info from a barcode.

## License
Licensed under a [MIT License](./LICENSE)